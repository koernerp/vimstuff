:colorscheme peaksea
call plug#begin('~/.vim/plugged')
Plug 'arsenerei/vim-sayid'
Plug 'kien/rainbow_parentheses.vim'
Plug 'dpelle/vim-LanguageTool'
"Plug 'sjl/gundo.vim' " look at this some day (requires pathogen I think)
"Plug 'vim-latex/vim-latex'
"Plug 'lervag/vimtex' " sucks
call plug#end()
" weird hack that seems to work on my terminal
:set background=light
:set background=dark
":colorscheme default
:set hlsearch
:set number
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
syntax on
au BufNewFile,BufRead *.clj,*.cljs setfiletype clojure
au BufNewFile,BufRead *.pl setfiletype prolog
"au BufNewFile,BufRead *.clj,*.cljs,*.pl call PareditInitBuffer()
au BufNewFile,BufRead *.clj,*.cljs call PareditInitBuffer()
autocmd BufRead *.clj try | silent! Require | catch /^Fireplace/ | endtry
let mapleader = "_"
execute pathogen#infect()
"au VimEnter * RainbowParenthesesToggle 
autocmd VimEnter *       RainbowParenthesesToggle
autocmd Syntax   clojure RainbowParenthesesLoadRound
autocmd Syntax   clojure RainbowParenthesesLoadSquare
autocmd Syntax   clojure RainbowParenthesesLoadBraces

let g:languagetool_jar='/home/philipp/Downloads/LanguageTool-4.8/languagetool-commandline.jar'
"set statusline+=%{g:Catium()}
"set laststatus=2
"let g:catium#trail=3
"let g:catium#body=4
"let g:catium#face=2
"let g:catium#space=1

" default value seems to be 100
let g:paredit_matchlines = 200
nnoremap <Leader>e :e <C-R>=expand('%:p:h') . '/'<CR>
nnoremap <Leader>E :tabe <C-R>=expand("%:p:h") . "/" <CR>

let g:vorlesung=0
command Vorlesung let g:vorlesung=1
command Work let g:vorlesung=0

map <F2> :Eval<CR>
"inoremap <expr> <F3> g:vorlesung==1 ? "moy%$a (do <ESC>mipc!!`ii ; => <ESC>`o" : "cpp"
"map <expr> <F3> g:vorlesung==0 ? "cpp" : "mo%muA ;<ESC>`uf;d$`oy%%$a <ESC>mia(do <ESC>p`[%a)<ESC>c!!`ii; => <ESC>`o"
map <expr> <F3> g:vorlesung==0 ? "cpp" : ":set paste<CR>mo%muA ;<ESC>`uf;d$`oy%%$a <ESC>mia(do <ESC>p`[%a)<ESC>c!!`ii; => <ESC>`o:set nopaste<CR>"
map <expr> <F4> g:vorlesung==0 ? "cp$" : ":set paste<CR>moA ;<ESC>`of;d$`oy$$a <ESC>mia(do <ESC>pa)<ESC>c!!`ii; => <ESC>`o:set nopaste<CR>"
"map <F3> cpp
"map <F4> cp$
map <F5> cmm
nnoremap <F6> :set nonumber!<CR>
let g:tex_indent_brace=0
let g:tex_indent_and=0

:set dictionary+=/usr/share/dict/words
let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['white',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

au BufNewFile,BufRead *.tex set spell 
au BufNewFile,BufRead *.tex hi SpellBad cterm=underline
